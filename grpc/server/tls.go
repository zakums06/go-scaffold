package server

import (
	"crypto/tls"
	"io/ioutil"

	"go.uber.org/zap"

	"golang.org/x/net/http2"
)

// GetTLSConfig for tls
func GetTLSConfig(pemPath, keyPath string) *tls.Config {
	// read cert file
	crt, _ := ioutil.ReadFile(pemPath)
	key, _ := ioutil.ReadFile(keyPath)
	pair, err := tls.X509KeyPair(crt, key)
	if err != nil {
		zap.L().Error("TLS KeyPair err", zap.String("error", err.Error()))
	}
	return &tls.Config{
		Certificates: []tls.Certificate{pair},
		NextProtos:   []string{http2.NextProtoTLS},
	}
}
