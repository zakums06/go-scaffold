package server

import (
	"context"
	"fmt"
	"gitee.com/zakums06/go-scaffold/db"
	"net"
	"strings"

	"google.golang.org/grpc/peer"
)

// PaginationParam for Pagination base params
type PaginationParam struct {
	Page    int    `form:"page,default=1" json:"page" binding:"max=100,min=1"`
	Size    int    `form:"size,default=10" json:"size" binding:"max=1000,min=1"`
	Order   string `form:"order" json:"order" binding:"lt=32"`
	OrderBy string `form:"order_by" json:"order_by" binding:"lt=256"`
}

// BindPagination for proto pagination
func BindPagination(r Pagination) *PaginationParam {
	p := &PaginationParam{}
	page := r.GetPage()
	if page == 0 {
		p.Page = db.DefaultPage
	} else if page > db.MaxPage {
		p.Page = db.MaxPage
	} else {
		p.Page = int(page)
	}
	size := r.GetSize()
	if size == 0 {
		p.Size = db.DefaultPageSize
	} else if size > db.MaxPageSize {
		p.Size = db.DefaultPageSize
	} else {
		p.Size = int(size)
	}
	p.Order = r.GetOrder()
	p.OrderBy = r.GetOrderBy()
	return p
}

// GetClientIP export
func GetClientIP(ctx context.Context) (string, error) {
	pr, ok := peer.FromContext(ctx)
	if !ok {
		return "", fmt.Errorf("GetClientIP invoke FromContext() failed")
	}
	if pr.Addr == net.Addr(nil) {
		return "", fmt.Errorf("GetClientIP peer.Addr is nil")
	}

	addrs := strings.Split(pr.Addr.String(), ":")
	if addrs[0] == "[" {
		return "localhost", nil
	}
	return addrs[0], nil
}
