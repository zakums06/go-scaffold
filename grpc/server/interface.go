package server

import (
	"context"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"google.golang.org/grpc"
)

// Register for gRPC service register
type Register interface {
	RegisterGRPC(g *grpc.Server)
	RegisterHTTP(c context.Context, gwmux *runtime.ServeMux, endpoint string, opts []grpc.DialOption) error
}

// Pagination for proto request
type Pagination interface {
	GetPage() int32
	GetSize() int32
	GetOrder() string
	GetOrderBy() string
}
