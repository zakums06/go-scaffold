/**
 * @Author ZoeAllen
 * @create 2020/8/25 3:27 下午
 */

package server

import "time"

var defaultServerOptions = Options{
	Name:               "server",
	Host:               "0.0.0.0",
	Port:               8080,
	SSLCrtFile:         "",
	SSLKeyFile:         "",
	DisableCORS:        false,
	ReadTimeout:        time.Duration(30) * time.Second,
	ReadHeaderTimeout:  time.Duration(30) * time.Second,
	WriteTimeout:       time.Duration(30) * time.Second,
	MaxHeaderBytes:     1 << 20,
	CORSAllowedMethods: []string{"GET", "POST", "PUT", "PATCH", "DELETE", "HEAD"},
	CORSAllowedOrigins: []string{"*"},
	CORSAllowedHeaders: []string{"Origin", "Content-Length", "Content-Type", "Authorization", "X-Requested-With"},
}

type Option func(options *Options)

// Options for http server
type Options struct {
	Name               string
	Host               string
	Port               int
	SSLCrtFile         string
	SSLKeyFile         string
	DisableCORS        bool
	ReadTimeout        time.Duration
	ReadHeaderTimeout  time.Duration
	WriteTimeout       time.Duration
	MaxHeaderBytes     int
	CORSAllowedMethods []string
	CORSAllowedOrigins []string
	CORSAllowedHeaders []string
}
