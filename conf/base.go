package conf

// Config instance with default value in global use
var Config = &MyConfig{
	Name:     "APP",
	LogFile:  "./logs/run.log",
	LogLevel: "INFO",
	// log file max size default to 100MB
	LogMaxSize:   100,
	LogMaxBackup: 10,
	LogMaxAge:    365,
	LogCompress:  false,
	EnableDebug:  true,
	EnableRedis:  false,
	EnableMySQL:  false,
}

// MyConfig export
type MyConfig struct {
	Name     string
	LogFile  string
	LogLevel string
	// megabytes for lumberjack rotate log file size
	LogMaxSize   int
	LogMaxBackup int
	LogMaxAge    int
	LogCompress  bool
	// SSL file path
	EnableDebug  bool
	EnableRedis  bool
	EnableMySQL  bool
	RedisConfig  RedisConfig
	MySQLConfig  MySQLConfig
	ServerConfig ServerConfig
}

// RedisConfig struct
type RedisConfig struct {
	Host      string `default:"127.0.0.1"`
	Port      string `default:"6379"`
	MaxIdle   int
	MaxActive int
	Password  string
	Db        int `default:"0"`

	// timeout millisecond
	ConnectionTimeout int `default:"30000"`

	// timeout millisecond
	ReadTimeout int `default:"5000"`

	// timeout millisecond
	WriteTimeout int `default:"5000"`

	// timeout second
	IdleTimeout int `default:"300"`
}

// MySQLConfig class
type MySQLConfig struct {
	Host     string `default:"127.0.0.1"`
	Port     string `default:"3306"`
	Name     string
	Username string
	Password string
	// connection pool
	MaxOpenConns int
	MaxIdleConns int
	// seconds of maximum amount a connection may be reused
	ConnMaxLifetime int
	Charset         string
	// seconds
	Timeout int
}

// ServerConfig struct
type ServerConfig struct {
	Host       string
	Port       int
	SSLName    string
	SSLCrtFile string
	SSLKeyFile string
}
