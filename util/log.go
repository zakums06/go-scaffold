package util

import (
	"gitee.com/zakums06/go-scaffold/conf"
	"log"
	"os"
	"path/filepath"
	"strings"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
)

func getLogLevel(s string) zapcore.Level {
	s = strings.TrimSpace(s)
	level, err := zapcore.ParseLevel(s)
	if err != nil {
		level = zap.InfoLevel
	}
	return level
}

func InitLog(conf *conf.MyConfig) *zap.Logger {
	if conf.EnableDebug {
		return InitDevLog(true, conf.LogFile)
	}
	return InitRotateLog(conf)
}

// InitDevLog in global
func InitDevLog(debug bool, logFile string) *zap.Logger {
	var loggerConfig zap.Config
	if debug {
		// dev
		loggerConfig = zap.NewDevelopmentConfig()
	} else {
		logPath := filepath.Dir(logFile)
		if !CheckPathExist(logPath) {
			println("InitLog InitDevLog log path:", logPath)
			_ = os.MkdirAll(logPath, 0755)
		}
		if !CheckPathExist(logFile) {
			_, _ = os.Create(logFile)
		}
		// prod
		loggerConfig = zap.NewProductionConfig()
		loggerConfig.OutputPaths = []string{logFile}
		loggerConfig.ErrorOutputPaths = []string{logFile}
		loggerConfig.EncoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	}
	logger, err := loggerConfig.Build()
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		_ = logger.Sync()
	}()
	zap.ReplaceGlobals(logger)
	// defer undo()
	return logger
}

// InitRotateLog export
func InitRotateLog(conf *conf.MyConfig) *zap.Logger {
	logPath := filepath.Dir(conf.LogFile)
	if !CheckPathExist(logPath) {
		println("InitRotateLog create log path:", logPath)
		_ = os.MkdirAll(logPath, 0755)
	}
	if !CheckPathExist(conf.LogFile) {
		_, _ = os.Create(conf.LogFile)
	}
	loggerConfig := zap.NewProductionEncoderConfig()
	loggerConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	// init rotate hook
	hook := &lumberjack.Logger{
		Filename:   conf.LogFile,
		MaxSize:    conf.LogMaxSize, // megabytes
		MaxBackups: conf.LogMaxBackup,
		MaxAge:     conf.LogMaxAge,
		Compress:   conf.LogCompress,
	}

	// defer hook.Close()

	w := zapcore.AddSync(hook)
	core := zapcore.NewCore(
		zapcore.NewJSONEncoder(loggerConfig),
		w,
		getLogLevel(conf.LogLevel),
	)
	logger := zap.New(core)
	zap.ReplaceGlobals(logger)
	return logger
}
