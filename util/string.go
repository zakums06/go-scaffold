package util

import (
	"bytes"
	"io/ioutil"
	"strings"

	"golang.org/x/text/encoding"
	"golang.org/x/text/encoding/simplifiedchinese"
	"golang.org/x/text/transform"
)

var gbkEncoder *encoding.Encoder
var gbkDecoder *encoding.Decoder

func init() {
	gbkEncoder = simplifiedchinese.GB18030.NewEncoder()
	gbkDecoder = simplifiedchinese.GB18030.NewDecoder()
}

// ConcatStrings from string array
func ConcatStrings(arr ...[]string) []string {
	var size int
	for _, v := range arr {
		size += len(v)
	}
	temp := make([]string, size)
	var i int
	for _, v := range arr {
		i += copy(temp[i:], v)
	}
	return temp
}

// IndexOfString for slice string find substr
func IndexOfString(s []string, substr string) int {
	for i, v := range s {
		if strings.Compare(substr, v) == 0 {
			return i
		}
	}
	return -1
}

// UTF82GBK : transform UTF8 rune into GBK byte array
func UTF82GBK(s string) ([]byte, error) {
	return ioutil.ReadAll(transform.NewReader(bytes.NewReader([]byte(s)), gbkEncoder))
}

// GBK2UTF8 : transform  GBK byte array into UTF8 string
func GBK2UTF8(s []byte) (string, error) {
	bytes, err := ioutil.ReadAll(transform.NewReader(bytes.NewReader(s), gbkDecoder))
	return string(bytes), err
}
