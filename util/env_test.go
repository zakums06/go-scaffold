/**
 * @Author ZoeAllen
 * @create 2020/12/18 5:19 下午
 */

package util

import (
	"os"
	"testing"
)

type TestConfig struct {
	Alias   []string `json:"alias" sep:","`
	Name    string   `json:"name" binding:"required"`
	Rank    int      `json:"rank"`
	Score32 float32  `json:"score_32"`
	Score64 float64  `json:"score_64" default:"100"`
	Match   bool     `json:"match" default:"true"`
}

func TestInitStructFromEnv(t *testing.T) {
	// set env
	os.Setenv("TESTCONFIG_ALIAS", "a,b,c")
	os.Setenv("TESTCONFIG_NAME", "test")
	os.Setenv("TESTCONFIG_RANK", "1")
	os.Setenv("TESTCONFIG_SCORE_32", "1.1")
	// os.Setenv("TESTCONFIG_SCORE_64", "1.2")
	// os.Setenv("TESTCONFIG_MATCH", "true")
	cfg := TestConfig{}
	cfg = InitStructFromEnv(cfg).Interface().(TestConfig)
	t.Logf("%#v\n", cfg)
}

func TestPrintEnvKey(t *testing.T) {
	cfg := TestConfig{}
	PrintEnvKey(cfg)
}
