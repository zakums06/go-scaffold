package util

import (
	"time"
)

// NanoGap export
const NanoGap = 1000 * 1000

const timeLayout = "2006-01-02 15:04:05"

// CurrentMilliseconds TL;DR
func CurrentMilliseconds() int64 {
	return time.Now().UnixNano() / NanoGap
}

// CurrentTime export
func CurrentTime() string {
	return time.Now().Format(timeLayout)
}

// CalcCurrentTimeGap d = duration string
func CalcCurrentTimeGap(d string) (time.Time, error) {
	now := time.Now()
	gap, err := time.ParseDuration(d)
	if err != nil {
		return time.Time{}, err
	}
	return now.Add(gap), nil
}

// BeginningOfMonth export
func BeginningOfMonth(t time.Time) time.Time {
	return time.Date(t.Year(), t.Month(), 1, 0, 0, 0, 0, t.Location())
}

// EndOfMonth export
func EndOfMonth(t time.Time) time.Time {
	return BeginningOfMonth(t).AddDate(0, 1, 0).Add(-time.Second)
}

// ToDateTimeString TL;DR
func ToDateTimeString(i interface{}, s string) string {
	switch v := i.(type) {
	case time.Time:
		if !v.IsZero() {
			return v.Format(s)
		}
	default:
		return ""
	}
	return ""
}
