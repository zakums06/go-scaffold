package util

import (
	"os"
	"path/filepath"
)

// CheckFileExist for file
func CheckFileExist(filePath string) (exist bool) {
	if _, err := os.Stat(filePath); os.IsNotExist(err) {
		exist = false
	} else {
		exist = true
	}
	return
}

// CreateDirPath make -p
func CreateDirPath(dirPath string) error {
	var err error
	if _, err = os.Stat(dirPath); os.IsNotExist(err) {
		err = os.MkdirAll(dirPath, os.ModePerm)
	}
	return err
}

// CreateFilePath make dir by file
func CreateFilePath(filePath string) error {
	parentPath := filepath.Dir(filePath)
	return CreateDirPath(parentPath)
}

// CheckPathExist check path exist
func CheckPathExist(path string) bool {
	_, err := os.Stat(path)
	if os.IsNotExist(err) {
		return false
	}
	return true
}
