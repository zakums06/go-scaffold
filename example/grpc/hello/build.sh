#!/bin/bash

# protoc to generate grpc stubs
protoc -I ./ -I ../../../third_party/googleapis \
        --go_out=. --go_opt=paths=source_relative \
        --go-grpc_out=. --go-grpc_opt=paths=source_relative \
        hello.proto

# protoc to generate grpc-gateway stubs
 protoc -I . -I ../../../third_party/googleapis --grpc-gateway_out . \
   --grpc-gateway_opt logtostderr=true \
   --grpc-gateway_opt paths=source_relative \
   --grpc-gateway_opt generate_unbound_methods=true \
   hello.proto