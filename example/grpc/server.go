/**
 * @Author ZoeAllen
 * @create 2020/8/19 4:35 下午
 */

package main

import (
	"context"
	"gitee.com/zakums06/go-scaffold/example/grpc/hello"
	"gitee.com/zakums06/go-scaffold/grpc/server"
	"gitee.com/zakums06/go-scaffold/util"
	"github.com/grpc-ecosystem/go-grpc-middleware/util/metautils"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"google.golang.org/grpc"
	"math/rand"
	"strings"
)

type CustomAuth struct {
	Client                  string
	Version                 string
	ApiKey                  string
	ApiSecret               string
	EnableTransportSecurity bool
}

func (t CustomAuth) GetRequestMetadata(ctx context.Context, in ...string) (map[string]string, error) {
	return map[string]string{
		"client":     t.Client,
		"version":    t.Version,
		"API-Key":    t.ApiKey,
		"API-Secret": t.ApiSecret,
	}, nil
}

func (t CustomAuth) RequireTransportSecurity() bool {
	return t.EnableTransportSecurity
}

type helloService struct {
	hello.UnimplementedHelloServer
}

// HelloService export
var HelloService = &helloService{}

func (s helloService) SayHi(ctx context.Context, req *hello.SayHiRequest) (*hello.SayHiResponse, error) {
	headers := metautils.ExtractIncoming(ctx)
	for k, v := range headers {
		println(k, strings.Join(v, ","))
	}
	resp := &hello.SayHiResponse{}
	resp.Text = "hi " + req.Name
	resp.MagicNumber = rand.Int31()
	resp.Number = hello.MagicNumber_ONE
	return resp, nil
}

// RegisterGRPC for gRPC
func (s helloService) RegisterGRPC(g *grpc.Server) {
	hello.RegisterHelloServer(g, HelloService)
}

// RegisterHTTP for http
func (s helloService) RegisterHTTP(ctx context.Context, gwmux *runtime.ServeMux, endpoint string, opts []grpc.DialOption) error {
	return hello.RegisterHelloHandlerFromEndpoint(ctx, gwmux, endpoint, opts)
}

func main() {
	util.InitDevLog(true, "")
	s := server.BaseServer{
		Name:             "test",
		Host:             "0.0.0.0",
		Port:             8000,
		EnableHTTPServer: true,
	}
	s.Services = []server.Register{
		HelloService,
	}
	s.Start(func(options *server.Options) {
		options.UseEnumNumbers = true
	})
}
