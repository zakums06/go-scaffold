package db

import (
	"errors"
	"fmt"
	"gitee.com/zakums06/go-scaffold/conf"
	"github.com/gomodule/redigo/redis"
	"go.uber.org/zap"
	"xorm.io/xorm"
)

var ErrorConnNameExist = errors.New("given conn name exist")

// Redis client in global
var Redis *redis.Pool

// MySQL client in global
var MySQL *xorm.Engine

var redisGroup map[string]*redis.Pool

func init() {
	redisGroup = make(map[string]*redis.Pool, 0)
}

// InitClient db connections
// call this func after conf.Config loaded
// after InitClient success, db.Redis / db.MySQL can use in global context
func InitClient() {
	// init redis
	if conf.Config.EnableRedis {
		Redis = GetRedisClient(conf.Config.RedisConfig)
	}
	// init mysql
	if conf.Config.EnableMySQL {
		MySQL = GetMySQLClient(conf.Config.MySQLConfig)
	}
}

func GetRedisPool(name string) *redis.Pool {
	if conn, ok := redisGroup[name]; ok {
		return conn
	}
	panic(fmt.Sprintf("GetRedisPool name:[%s] not found", name))
}

//
// SetRedisPool
// @Description: this is not goroutine safe, call the func in main process
// @param name
// @param cfg
// @return error
//
func SetRedisPool(name string, cfg conf.RedisConfig) error {
	if _, ok := redisGroup[name]; ok {
		zap.L().Error("SetRedisPool conn name exist", zap.String("name", name))
		return ErrorConnNameExist
	}
	redisGroup[name] = InitRedisPool(cfg)
	return nil
}
