package db

import (
	"gitee.com/zakums06/go-scaffold/conf"
	"github.com/gomodule/redigo/redis"
	"sync"
	"time"

	"go.uber.org/zap"
)

var redisOnce sync.Once
var redisInstance *redis.Pool

func InitRedisPool(cfg conf.RedisConfig) *redis.Pool {
	addr := cfg.Host + ":" + cfg.Port
	zap.L().Info("InitRedisPool", zap.String("addr", addr))
	return &redis.Pool{
		MaxIdle:   cfg.MaxIdle,
		MaxActive: cfg.MaxActive,
		Dial: func() (redis.Conn, error) {
			conn, err := redis.Dial("tcp", addr,
				redis.DialPassword(cfg.Password),
				redis.DialDatabase(cfg.Db),
				redis.DialConnectTimeout(time.Duration(cfg.ConnectionTimeout)*time.Millisecond),
				redis.DialReadTimeout(time.Duration(cfg.ReadTimeout)*time.Millisecond),
				redis.DialWriteTimeout(time.Duration(cfg.WriteTimeout)*time.Millisecond))
			if err != nil {
				zap.L().Error(err.Error())
				return nil, err
			}
			return conn, nil
		},
		// Use the TestOnBorrow function to check the health of an idle connection
		// before the connection is returned to the application.
		TestOnBorrow: func(conn redis.Conn, t time.Time) error {
			if time.Since(t) < time.Minute {
				return nil
			}
			_, err := conn.Do("PING")
			return err
		},
		IdleTimeout: time.Duration(cfg.IdleTimeout) * time.Second,
		// If Wait is true and the pool is at the MaxActive limit,
		// then Get() waits for a connection to be returned to the pool before returning
		Wait: true,
	}
}

// GetRedisClient of redis pool
func GetRedisClient(cfg conf.RedisConfig) *redis.Pool {
	redisOnce.Do(func() {
		redisInstance = InitRedisPool(cfg)
	})
	return redisInstance
}
